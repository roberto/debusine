#!/bin/sh

set -e

if [ -e debusine/project/settings/local.py ]; then
    echo "ERROR: You already have a configuration file (debusine/project/settings/local.py)" >&2
    exit 1
fi

if [ -e data/debusine.sqlite ]; then
    echo "ERROR: You already have a database file (data/debusine.sqlite)" >&2
    exit 1
fi

if [ ! -e debusine/project/settings/local.py.sample ]; then
    echo "ERROR: are you at the root of the debusine repository?"
    echo "USAGE: ./bin/$0"
    exit 1
fi

echo ">>> Ensuring we have the required packages"
packages="python3-django python3-requests python3-django-debug-toolbar python3-debian python3-yaml python3-selenium chromium-driver postgresql"
if ! dpkg-query -W $packages >/dev/null; then
    echo ">>> Installing the required packages with “sudo apt install”"
    sudo apt install $packages
fi
version=$(dpkg-query -W -f'${Version}' python3-django)
if dpkg --compare-versions $version lt 2:2.2; then
    echo "WARNING: you need python3-django >= 2:2.2"
    echo "Trying to install it from buster-backports"
    sudo apt install python3-django/buster-backports
fi

echo ">>> Installing a configuration file"
cp debusine/project/settings/local.py.sample debusine/project/settings/local.py

echo ">>> Configuring postgresql user and databases"
sudo -u postgres createuser -d $USER || true
if ! sudo -u postgres psql -A -l | grep -q '^debusine|'; then
    sudo -u postgres createdb -O $USER -E UTF8 debusine
fi

# echo ">>> Downloading a pre-built sample database file"
# Note: when https://gitlab.com/gitlab-org/gitlab-ce/issues/45697 will be
# fixed, we should be able to use 
# https://salsa.debian.org/qa/debusine/-/jobs/artifacts/master/raw/data/debusine.sqlite?job=sample-database
# url=$(bin/sample-database-url)
# if [ -n "$url" ]; then
#    wget "$url" -O data/debusine.sqlite
# else
#    echo "ERROR: unable to find sample database url (bin/sample-database-url returned nothing)"
#    exit 1
# fi
