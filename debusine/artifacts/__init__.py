# Copyright 2019 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.
"""
The `artifacts` application, part of Debusine.

This application is the core of the debusine project since it handles
the `Artifact` data model used to store all the files, those submitted
as input and those generated as output.
"""
